import 'package:flutter/material.dart';

void main() {
  runApp(
    MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(
        appBar: AppBar(
          backgroundColor: Colors.amberAccent,
          title: const Text("I Am Poor"),
          centerTitle: true,
        ),
        body: const Center(
          child: Image(
            image: AssetImage('images/paisaje1.jpg'),
          ),
        ),
      ),
    ),
  );
}
